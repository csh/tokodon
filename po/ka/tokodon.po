# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the tokodon package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: tokodon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-03 00:47+0000\n"
"PO-Revision-Date: 2023-01-01 07:29+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: abstractaccount.cpp:408
#, kde-format
msgid "Could not follow account"
msgstr "ანგარიშზე მიყოლის შეცდომა"

#: abstractaccount.cpp:409
#, kde-format
msgid "Could not unfollow account"
msgstr "ანგარიშზე მიყოლის გაუქმების შეცდომა"

#: abstractaccount.cpp:410
#, kde-format
msgid "Could not block account"
msgstr "ანგარიშის დაბლოკვის შეცდომა"

#: abstractaccount.cpp:411
#, kde-format
msgid "Could not unblock account"
msgstr "ანგარიშის განბლოკვის შეცდომა"

#: abstractaccount.cpp:412
#, kde-format
msgid "Could not mute account"
msgstr "ანგარიშის დადუმების შეცდომა"

#: abstractaccount.cpp:413
#, kde-format
msgid "Could not unmute account"
msgstr "ანგარიშის დადუმების მოხსნის შეცდომა"

#: abstractaccount.cpp:414
#, kde-format
msgid "Could not feature account"
msgstr "ანგარიშის მიმაგრების შეცდომა"

#: abstractaccount.cpp:415
#, kde-format
msgid "Could not unfeature account"
msgstr "ანგარიშის მიმაგრების მოხსნის შეცდომა"

#: abstractaccount.cpp:416
#, kde-format
msgid "Could not edit note about an account"
msgstr "ანგარიშის შესახებ შენიშვნის ჩასწორების შეცდომა"

#: abstracttimelinemodel.cpp:134
#, kde-format
msgctxt "hour:minute"
msgid "%1:%2"
msgstr "%1:%2"

#: abstracttimelinemodel.cpp:138
#, kde-format
msgid "%1h"
msgstr "%1სთ"

#: abstracttimelinemodel.cpp:140
#, kde-format
msgid "%1d"
msgstr "%1დღ"

#: accountmodel.cpp:51 maintimelinemodel.cpp:27
#, kde-format
msgid "Loading"
msgstr "ჩატვირთვა"

#: content/ui/AccountInfo.qml:46
#, kde-format
msgid "Follows you"
msgstr "მოგყვებათ"

#: content/ui/AccountInfo.qml:85
#, kde-format
msgid "Requested"
msgstr "Მოთხოვნილი"

#: content/ui/AccountInfo.qml:88
#, kde-format
msgid "Following"
msgstr "გამოწერილია"

#: content/ui/AccountInfo.qml:90
#, kde-format
msgid "Follow"
msgstr "გამოწერა"

#: content/ui/AccountInfo.qml:115
#, kde-format
msgid "Stop notifying me when %1 posts"
msgstr "%1-ის პოსტების შესახებ გაფრთხილებების შეწყვეტა"

#: content/ui/AccountInfo.qml:117
#, kde-format
msgid "Notify me when %1 posts"
msgstr "შემატყობინე %1-ის პოსტების შესახებ"

#: content/ui/AccountInfo.qml:133
#, kde-format
msgid "Hide boosts from %1"
msgstr "%1-დან აწევების დამალვა"

#: content/ui/AccountInfo.qml:135
#, kde-format
msgid "Stop hiding boosts from %1"
msgstr "%1-დან აწევების შეწყვეტა"

#: content/ui/AccountInfo.qml:151
#, kde-format
msgid "Stop featuring on profile"
msgstr "პროფილზე ჩვენების შეწყვეტა"

#: content/ui/AccountInfo.qml:153
#, kde-format
msgid "Feature on profile"
msgstr "პროფილზე ჩვენება"

#: content/ui/AccountInfo.qml:169
#, kde-format
msgid "Stop muting"
msgstr "დადუმების შეჩერება"

#: content/ui/AccountInfo.qml:171
#, kde-format
msgid "Mute"
msgstr "დადუმება"

#: content/ui/AccountInfo.qml:187
#, kde-format
msgid "Stop blocking"
msgstr "დაბლოკვის შეჩერება"

#: content/ui/AccountInfo.qml:189
#, kde-format
msgid "Block"
msgstr "დაბლოკვა"

#: content/ui/AccountInfo.qml:273
#, kde-format
msgid "Note"
msgstr "შენიშვნა"

#: content/ui/AccountInfo.qml:281
#, kde-format
msgid "Saved"
msgstr "შენახულია"

#: content/ui/AccountInfo.qml:295
#, kde-format
msgid "Click to add a note"
msgstr "შენიშვნის დასამატებლად დააწკაპუნეთ"

#: content/ui/AccountInfo.qml:361
#, kde-format
msgid "%1 toots"
msgstr "%1 ტუტი"

#: content/ui/AccountInfo.qml:365
#, kde-format
msgid "%1 followers"
msgstr "%1 მიმდევარი"

#: content/ui/AccountInfo.qml:370
#, kde-format
msgid "%1 following"
msgstr "%1 -ს მისდევთ"

#: content/ui/AuthorizationPage.qml:15
#, kde-format
msgid "Authorization"
msgstr "ავტორიზაცია"

#: content/ui/AuthorizationPage.qml:29
#, kde-format
msgid "Authorize Tokodon to act on your behalf"
msgstr "Tokodon -ისთვის ავტორიზაციის მინიჭება, რომ თქვენს მაგიერ იმოქმედოს"

#: content/ui/AuthorizationPage.qml:41
#, kde-format
msgid "To continue, please open the following link and authorize Tokodon: %1"
msgstr "გასაგრძელებლად გახსენით ეს ბმული და მიანიჭეთ ავტორიზაცია Tokodon-ს: %1"

#: content/ui/AuthorizationPage.qml:57 content/ui/AuthorizationPage.qml:80
#, kde-format
msgid "Copy link"
msgstr "Ბმულის კოპირება"

#: content/ui/AuthorizationPage.qml:61 content/ui/AuthorizationPage.qml:72
#, kde-format
msgid "Open link"
msgstr "ბმულის გახსნა"

#: content/ui/AuthorizationPage.qml:94
#, kde-format
msgid "Enter token:"
msgstr "შეიყვანეთ კოდი:"

#: content/ui/AuthorizationPage.qml:102 content/ui/LoginPage.qml:52
#, kde-format
msgid "Continue"
msgstr "გაგრძელება"

#: content/ui/AuthorizationPage.qml:105
#, kde-format
msgid "Please insert the generated token."
msgstr "ჩასვით გენერირებული კოდი."

#: content/ui/ConversationPage.qml:11
#, kde-format
msgid "Conversations"
msgstr "საუბრები"

#: content/ui/FollowDelegate.qml:42 content/ui/FollowDelegate.qml:79
#: notificationhandler.cpp:27
#, kde-format
msgid "%1 followed you"
msgstr "%1 მოგყვებათ"

#: content/ui/FullScreenImage.qml:43
#, kde-format
msgid "Zoom in"
msgstr "გადიდება"

#: content/ui/FullScreenImage.qml:54
#, kde-format
msgid "Zoom out"
msgstr "დაპატარავება"

#: content/ui/FullScreenImage.qml:65
#, kde-format
msgid "Rotate left"
msgstr "მარცხნივ მოტრიალება"

#: content/ui/FullScreenImage.qml:72
#, kde-format
msgid "Rotate right"
msgstr "მარჯვნივ მოტრიალება"

#: content/ui/FullScreenImage.qml:79
#, kde-format
msgid "Save as"
msgstr "შენახვა, როგორც"

#: content/ui/FullScreenImage.qml:91
#, kde-format
msgid "Close"
msgstr "დახურვა"

#: content/ui/FullScreenImage.qml:202
#, kde-format
msgid "Previous image"
msgstr "წინა გამოსახულება"

#: content/ui/FullScreenImage.qml:217
#, kde-format
msgid "Next image"
msgstr "შემდეგი გამოსახულება"

#: content/ui/LoginPage.qml:14
#, kde-format
msgid "Login"
msgstr "შესვლა"

#: content/ui/LoginPage.qml:28
#, kde-format
msgid "Welcome to Tokodon"
msgstr "მოგესალმებით Tokodon-ში"

#: content/ui/LoginPage.qml:33
#, kde-format
msgid "Instance Url:"
msgstr "გაშვებული ასლის URL:"

#: content/ui/LoginPage.qml:40
#, kde-format
msgid "Username:"
msgstr "მომხმარებელი:"

#: content/ui/LoginPage.qml:47
#, kde-format
msgid "Ignore ssl errors"
msgstr "SSL-ის შეცდომების იგნორი"

#: content/ui/LoginPage.qml:55
#, kde-format
msgid "Instance URL and username must not be empty!"
msgstr "გაშვებული ასლის ბმული და მომხმარებლის სახელი ცარიელი არ შეიძლება იყოს!"

#: content/ui/main.qml:103
#, kde-format
msgid "Home"
msgstr "საწყისი"

#: content/ui/main.qml:117 content/ui/NotificationPage.qml:13
#, kde-format
msgid "Notifications"
msgstr "გაფრთხილებები"

#: content/ui/main.qml:127
#, kde-format
msgid "Local"
msgstr "ლოკალური"

#: content/ui/main.qml:140
#, kde-format
msgid "Global"
msgstr "გლობალური"

#: content/ui/main.qml:154
#, kde-format
msgid "Conversation"
msgstr "საუბარი"

#: content/ui/NotificationPage.qml:23 content/ui/TimelinePage.qml:53
#, kde-format
msgid "Toot"
msgstr "ტუტი"

#: content/ui/NotificationPage.qml:35
#, kde-format
msgctxt "Show all notifications"
msgid "All"
msgstr "ყველა"

#: content/ui/NotificationPage.qml:45
#, kde-format
msgctxt "Show only mentions"
msgid "Mentions"
msgstr "მოხსენებები"

#: content/ui/NotificationPage.qml:108 content/ui/TimelinePage.qml:77
#, kde-format
msgid "Loading..."
msgstr "ჩატვირთვა..."

#: content/ui/PostDelegate.qml:91
#, kde-format
msgid "Filtered: %1"
msgstr ""

#: content/ui/PostDelegate.qml:95
#, kde-format
msgid "Show anyway"
msgstr ""

#: content/ui/PostDelegate.qml:117 notificationhandler.cpp:23
#, kde-format
msgid "%1 favorited your post"
msgstr "%1 ჩაინიშნა თქვენი პოსტი"

#: content/ui/PostDelegate.qml:119
#, kde-format
msgid "%1 edited a post"
msgstr "%1 -მა პოსტი ჩაასწორა"

#: content/ui/PostDelegate.qml:140
#, kde-format
msgid "Pinned entry"
msgstr "მიჭიკარტებული ჩანაწერი"

#: content/ui/PostDelegate.qml:161
#, kde-format
msgid "%1 boosted"
msgstr "%1 აწეულია"

#: content/ui/PostDelegate.qml:161 notificationhandler.cpp:31
#, kde-format
msgid "%1 boosted your post"
msgstr "%1 -მა აწია თქვენი პოსტი"

#: content/ui/PostDelegate.qml:178
#, kde-format
msgid "View profile"
msgstr "პროფილის ნახვა"

#: content/ui/PostDelegate.qml:226
#, kde-format
msgid "Show more"
msgstr "მეტის ჩვენება"

#: content/ui/PostDelegate.qml:347
#, kde-format
msgid "Not available"
msgstr ""

#: content/ui/PostDelegate.qml:375
#, kde-format
msgid "Media Hidden"
msgstr "მედია დამალულია"

#: content/ui/PostDelegate.qml:504
#, kde-format
msgctxt "Votes percentage"
msgid "%1%"
msgstr "%1%"

#: content/ui/PostDelegate.qml:528
#, kde-format
msgid "(No votes)"
msgstr "(ხმების გარეშე)"

#: content/ui/PostDelegate.qml:562
#, kde-format
msgid "Vote"
msgstr "ხმის მიცემა"

#: content/ui/PostDelegate.qml:587
#, kde-format
msgctxt "More than one reply"
msgid "1+"
msgstr "1+"

#: content/ui/PostDelegate.qml:600
#, kde-format
msgctxt "Reply to a post"
msgid "Reply"
msgstr "პასუხი"

#: content/ui/PostDelegate.qml:610
#, kde-format
msgctxt "Share a post"
msgid "Boost"
msgstr "გაძლიერება"

#: content/ui/PostDelegate.qml:620
#, kde-format
msgctxt "Like a post"
msgid "Like"
msgstr "მომწონს"

#: content/ui/Settings/AccountsCard.qml:22
#, kde-format
msgid "Accounts"
msgstr "ანგარიშები"

#: content/ui/Settings/AccountsCard.qml:32
#, kde-format
msgid "Account editor"
msgstr "ანგარიშების რედაქტორი"

#: content/ui/Settings/AccountsCard.qml:68
#, kde-format
msgid "Logout"
msgstr "გასვლა"

#: content/ui/Settings/AccountsCard.qml:91 content/ui/UserInfo.qml:77
#, kde-format
msgid "Add Account"
msgstr "ანგარიშის დამატება"

#: content/ui/Settings/GeneralCard.qml:18
#, kde-format
msgid "General"
msgstr "საერთო"

#: content/ui/Settings/GeneralCard.qml:23
#, kde-format
msgid "Show detailed statistics about posts"
msgstr "პოსტების შესახებ დეტალური სტატისტიკის ჩვენება"

#: content/ui/Settings/GeneralCard.qml:36
#, kde-format
msgid "Show link preview"
msgstr "ბმულის მინიატურის ჩვენება"

#: content/ui/Settings/NetworkProxyPage.qml:14
#, kde-format
msgctxt "@title:window"
msgid "Network Proxy"
msgstr "ქსელის პროქსი"

#: content/ui/Settings/NetworkProxyPage.qml:27
#: content/ui/Settings/SettingsPage.qml:37
#, kde-format
msgid "Network Proxy"
msgstr "ქსელის პროქსი"

#: content/ui/Settings/NetworkProxyPage.qml:30
#, kde-format
msgid "System Default"
msgstr "სისტემის ნაგულისხმები"

#: content/ui/Settings/NetworkProxyPage.qml:38
#, kde-format
msgid "HTTP"
msgstr "HTTP"

#: content/ui/Settings/NetworkProxyPage.qml:46
#, kde-format
msgid "Socks5"
msgstr "Socks5"

#: content/ui/Settings/NetworkProxyPage.qml:62
#, kde-format
msgid "Proxy Settings"
msgstr "პროქსის მორგება"

#: content/ui/Settings/NetworkProxyPage.qml:66
#, kde-format
msgid "Host"
msgstr "ჰოსტი"

#: content/ui/Settings/NetworkProxyPage.qml:78
#, kde-format
msgid "Port"
msgstr "პორტი"

#: content/ui/Settings/NetworkProxyPage.qml:98
#, kde-format
msgid "User"
msgstr "მომხმარებელი"

#: content/ui/Settings/NetworkProxyPage.qml:107
#, kde-format
msgid "Password"
msgstr "პაროლი"

#: content/ui/Settings/NetworkProxyPage.qml:127
#: content/ui/Settings/ProfileEditor.qml:356
#, kde-format
msgid "Apply"
msgstr "გამოყენება"

#: content/ui/Settings/ProfileEditor.qml:23
#, kde-format
msgid "Profile Editor"
msgstr "პროფილების რედაქტორი"

#: content/ui/Settings/ProfileEditor.qml:31 content/ui/TootComposer.qml:157
#, kde-format
msgid "Please choose a file"
msgstr "გთხოვთ, აირჩიოთ ფაილი"

#: content/ui/Settings/ProfileEditor.qml:116
#, kde-format
msgid "Display Name"
msgstr "საჩვენებელი სახელი"

#: content/ui/Settings/ProfileEditor.qml:129
#, kde-format
msgid "Bio"
msgstr "ბიო"

#: content/ui/Settings/ProfileEditor.qml:148
#, kde-format
msgid "Header"
msgstr "სათაური"

#: content/ui/Settings/ProfileEditor.qml:182
#, kde-format
msgid "PNG, GIF or JPG. At most 2 MB. Will be downscaled to 1500x500px"
msgstr "PNG, GIF ან JPG. მაქს 2 მბ. დაპატარავდება ზომებამდე 1500x500px"

#: content/ui/Settings/ProfileEditor.qml:195
#: content/ui/Settings/ProfileEditor.qml:257
#, kde-format
msgid "Delete"
msgstr "DELETE"

#: content/ui/Settings/ProfileEditor.qml:210
#, kde-format
msgid "Avatar"
msgstr "ავატარი"

#: content/ui/Settings/ProfileEditor.qml:275
#, kde-format
msgid "Default status privacy"
msgstr "სტატუსის ნაგულისხმები კონფიდენციალობა"

#: content/ui/Settings/ProfileEditor.qml:278
#, kde-format
msgid "Public post"
msgstr "საჯარო"

#: content/ui/Settings/ProfileEditor.qml:282
#, kde-format
msgid "Unlisted post"
msgstr "სიაში არაა"

#: content/ui/Settings/ProfileEditor.qml:286
#, kde-format
msgid "Followers-only post"
msgstr "მოგყვებათ"

#: content/ui/Settings/ProfileEditor.qml:290
#, kde-format
msgid "Direct post"
msgstr "პირადი შეტყობინება"

#: content/ui/Settings/ProfileEditor.qml:309
#, kde-format
msgid "Mark by default content as sensitive"
msgstr "შემცველობის საშიშად ნაგულისხმევად მონიშვნა"

#: content/ui/Settings/ProfileEditor.qml:317
#, kde-format
msgid "Require follow requests"
msgstr "გამოწერის მოთხოვების აუცილებლად მოთხოვნა"

#: content/ui/Settings/ProfileEditor.qml:325
#, kde-format
msgid "This is a bot account"
msgstr "ეს ბოტის ანგარიშია"

#: content/ui/Settings/ProfileEditor.qml:333
#, kde-format
msgid "Suggest account to others"
msgstr "ამ ანგარიშის სხვებისთვის შეთავაზება"

#: content/ui/Settings/ProfileEditor.qml:349
#, kde-format
msgid "Reset"
msgstr "თავიდან ჩართვა"

#: content/ui/Settings/SettingsPage.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "მორგება"

#: content/ui/Settings/SettingsPage.qml:55
#, kde-format
msgid "About Tokodon"
msgstr "Tokodon-ის შესახებ"

#: content/ui/Settings/SonnetCard.qml:24
#, kde-format
msgid "Spellchecking"
msgstr "მართლწერის შემოწმება"

#: content/ui/Settings/SonnetCard.qml:30
#, kde-format
msgid "Enable automatic spell checking"
msgstr "მართლწერის შემოწმება"

#: content/ui/Settings/SonnetCard.qml:42
#, kde-format
msgid "Ignore uppercase words"
msgstr "ზედა რეგისტრის მქონე სიტყვების გამოტოვება"

#: content/ui/Settings/SonnetCard.qml:54
#, kde-format
msgid "Ignore hyphenated words"
msgstr "გამოტოვების მქონე სიტყვების იგნორი"

#: content/ui/Settings/SonnetCard.qml:66
#, kde-format
msgid "Detect language automatically"
msgstr "ენების ავტომატური გამოცნობა"

#: content/ui/Settings/SonnetCard.qml:77
#, kde-format
msgid "Selected default language:"
msgstr "აირჩიეთ ნაგულისხმები ენა:"

#: content/ui/Settings/SonnetCard.qml:78
#, kde-format
msgid "None"
msgstr "არაფერი"

#: content/ui/Settings/SonnetCard.qml:96
#, kde-format
msgid "Additional spell checking languages"
msgstr "მართლწერის შემოწმების დამატებითი ენები"

#: content/ui/Settings/SonnetCard.qml:97
#, kde-format
msgid ""
"%1 will provide spell checking and suggestions for the languages listed here "
"when autodetection is enabled."
msgstr ""
"%1 წარმოგიდგენთ მართლწერის შემოწმებისა და მინიშნებებს აქ ჩამოთვლილი "
"ენებისთვის, როცა ავტომატური არჩევა ჩართულია."

#: content/ui/Settings/SonnetCard.qml:108
#, kde-format
msgid "Open Personal Dictionary"
msgstr "პირადი ლექსიკონის გახსნა"

#: content/ui/Settings/SonnetCard.qml:119
#, kde-format
msgctxt "@title:window"
msgid "Spell checking languages"
msgstr "მართლწერის შემოწმების ენები"

#: content/ui/Settings/SonnetCard.qml:129
#: content/ui/Settings/SonnetCard.qml:139
#, kde-format
msgid "Default Language"
msgstr "ნაგულისხმები ენა"

#: content/ui/Settings/SonnetCard.qml:151
#, kde-format
msgid "Spell checking dictionary"
msgstr "მართლწერის შემოწმების ლექსიკონი"

#: content/ui/Settings/SonnetCard.qml:158
#, kde-format
msgid "Add a new word to your personal dictionary…"
msgstr "დაამატეთ ახალი სიტყვა თქვენს პირად ლექსიკონში…"

#: content/ui/Settings/SonnetCard.qml:161
#, kde-format
msgctxt "@action:button"
msgid "Add word"
msgstr "სიტყვის დამატება"

#: content/ui/Settings/SonnetCard.qml:188
#, kde-format
msgid "Delete word"
msgstr "სიტყვის წაშლა"

#: content/ui/TootComposer.qml:13
#, kde-format
msgid "Write a new toot"
msgstr "დაწერეთ ახალი ტუტი"

#: content/ui/TootComposer.qml:21 content/ui/TootComposer.qml:221
#, kde-format
msgid "Content Warning"
msgstr "შემცველობის გაფრთხილება"

#: content/ui/TootComposer.qml:29
#, kde-format
msgid "What's new?"
msgstr "რა არის ახალი?"

#: content/ui/TootComposer.qml:125
#, kde-format
msgid "Make pool auto-exclusive"
msgstr "პულის ავტო-ექსკლუზიურად გახდომა"

#: content/ui/TootComposer.qml:133
#, kde-format
msgid "Choice %1"
msgstr "არჩევანი %1"

#: content/ui/TootComposer.qml:160
#, kde-format
msgid "Attach File"
msgstr "ფაილის მიმაგრება"

#: content/ui/TootComposer.qml:168
#, kde-format
msgid "Add Poll"
msgstr "დაამატე გამოკითხვა"

#: content/ui/TootComposer.qml:193
#, kde-format
msgid "Public"
msgstr "საჯარო"

#: content/ui/TootComposer.qml:198
#, kde-format
msgid "Unlisted"
msgstr "სიაში არაა"

#: content/ui/TootComposer.qml:203
#, kde-format
msgid "Private"
msgstr "პირადი"

#: content/ui/TootComposer.qml:208
#, kde-format
msgid "Direct Message"
msgstr "პირადი შეტყობინება"

#: content/ui/TootComposer.qml:213
#, kde-format
msgid "Visibility"
msgstr "ხილვადობა"

#: content/ui/TootComposer.qml:218
#, kde-format
msgctxt "Short for content warning"
msgid "cw"
msgstr "გაფრთხ"

#: content/ui/TootComposer.qml:230
#, kde-format
msgid "Send"
msgstr "გაგზავნა"

#: content/ui/UserInfo.qml:80
#, kde-format
msgid "Log in to an existing account"
msgstr "შედით არსებულ ანგარიშზე"

#: content/ui/UserInfo.qml:202
#, kde-format
msgid "Switch User"
msgstr "მომხმარებლის გადართვა"

#: content/ui/UserInfo.qml:214
#, kde-format
msgid "Add"
msgstr "დამატება"

#: content/ui/UserInfo.qml:225
#, kde-format
msgid "Configure"
msgstr "მორგება"

#: content/ui/UserInfo.qml:226
#, kde-format
msgid "Open Settings"
msgstr "პარამეტრების გახსნა"

#: conversationmodel.cpp:60
#, kde-format
msgid "Empty conversation"
msgstr "ცარიელი საუბარი"

#: conversationmodel.cpp:64
#, kde-format
msgid "%1 and %2"
msgstr "%1 და %2"

#: conversationmodel.cpp:66
#, kde-format
msgid "%2 and one other"
msgid_plural "%2 and %1 others"
msgstr[0] "%2 და კიდევ %1"
msgstr[1] "%2 და კიდევ %1"

#: filetransferjob.cpp:82
#, kde-format
msgctxt "Job heading, like 'Copying'"
msgid "Downloading"
msgstr "გადმოწერა"

#: filetransferjob.cpp:83
#, kde-format
msgctxt "The URL being downloaded/uploaded"
msgid "Source"
msgstr "წყარო"

#: filetransferjob.cpp:84
#, kde-format
msgctxt "The location being downloaded to"
msgid "Destination"
msgstr "დანიშნულება"

#: main.cpp:90
#, kde-format
msgid "Tokodon"
msgstr "Tokodon"

#: main.cpp:92
#, kde-format
msgid "Mastodon client"
msgstr "Mastodon-ის კლიენტი"

#: main.cpp:94
#, fuzzy, kde-format
#| msgid "© 2021 Carl Schwan, 2021 KDE Community"
msgid "© 2021-2023 Carl Schwan, 2021-2023 KDE Community"
msgstr "© 2021 Carl Schwan, 2021 KDE -ის საზოგადოება"

#: main.cpp:95
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:95
#, kde-format
msgid "Maintainer"
msgstr "წამყვანი პროგრამისტი"

#: main.cpp:96
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#: main.cpp:96
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: main.cpp:140
#, kde-format
msgid "Client for the decentralized social network: mastodon"
msgstr "დეცენტრალიზებული სოციალური მედიის კლიენტი: mastodon"

#: maintimelinemodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Home (%1)"
msgstr "საწყისი (%1)"

#: maintimelinemodel.cpp:31
#, kde-format
msgctxt "@title"
msgid "Home"
msgstr "საწყისი"

#: maintimelinemodel.cpp:34
#, kde-format
msgctxt "@title"
msgid "Local Timeline"
msgstr "ლოკალური დროის ხაზი"

#: maintimelinemodel.cpp:36
#, kde-format
msgctxt "@title"
msgid "Global Timeline"
msgstr "გლობალური დროის ხაზი"

#: notificationmodel.cpp:100
#, kde-format
msgid "Error occurred when fetching the latest notification."
msgstr "შეცდომა უახლესი შეტყობინების გამოთხოვისას."

#: profileeditor.cpp:183
#, kde-format
msgid "Image is too big"
msgstr "გამოსახულება მეტისმეტად დიდია"

#: profileeditor.cpp:196
#, kde-format
msgid "Unsupported image file. Only jpeg, png and gif are supported."
msgstr ""
"მხარდაუჭერელი გამოსახულების ტიპი. მხარდაჭერილია მხოლოდ jpeg, png და gif "
"ფაილები."

#: profileeditor.cpp:323
#, kde-format
msgid "Account details saved"
msgstr "ანგარშის დეტალები შენახულია"

#: searchmodel.cpp:114
#, kde-format
msgid "People"
msgstr "ხალხი"

#: searchmodel.cpp:116
#, kde-format
msgid "Hashtags"
msgstr "ჰეშტეგები"

#: searchmodel.cpp:118
#, kde-format
msgid "Post"
msgstr "პოსტი"

#: threadmodel.cpp:30
#, kde-format
msgctxt "@title"
msgid "Thread"
msgstr "ნაკადი"

#~ msgid "Refresh"
#~ msgstr "განახლება"

#~ msgid "Image View"
#~ msgstr "სურათის ხედი"

#~ msgid "Local Timeline"
#~ msgstr "ლოკალური დროის ხაზი"

#~ msgid "Global Timeline"
#~ msgstr "გლობალური დროის ხაზი"

#~ msgid "Add an account"
#~ msgstr "ახალი ანგარიშის დამატება"

#~ msgid "Options:"
#~ msgstr "პარამეტრები:"
